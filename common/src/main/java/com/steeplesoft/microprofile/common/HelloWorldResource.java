package com.steeplesoft.microprofile.common;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("/")
@Produces(MediaType.TEXT_PLAIN)
public class HelloWorldResource {
    @Inject
    private HelloWorldService service;

    @GET
    public String sayHello(@QueryParam("name") @DefaultValue("world") String name) {
        return service.sayHello(name);
    }
}
