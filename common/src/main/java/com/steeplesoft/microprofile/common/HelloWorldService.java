package com.steeplesoft.microprofile.common;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class HelloWorldService {
    public String sayHello(String name) {
        return "Hello, " + name;
    }
}
