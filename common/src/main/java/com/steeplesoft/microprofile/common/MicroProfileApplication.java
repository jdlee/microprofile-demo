package com.steeplesoft.microprofile.common;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.enterprise.context.ApplicationScoped;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped // Helidon-only
@ApplicationPath("/")
public class MicroProfileApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<>();
        set.add(HelloWorldResource.class);
        return Collections.unmodifiableSet(set);
    }
}
