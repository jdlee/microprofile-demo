package com.steeplesoft.microprofile.hammock;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ws.ament.hammock.bootstrap.Bootstrapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ServiceLoader;

public class HammockTest {
    private static Bootstrapper bootstrapper;
    @BeforeClass
    public static void setup() {
        bootstrapper = ServiceLoader.load(Bootstrapper.class).iterator().next();
        bootstrapper.start();
    }

    @AfterClass
    public static void shutdown() {
        if (bootstrapper != null) {
            bootstrapper.stop();
        }
    }

    @Test
    public void shouldSayWorld() throws URISyntaxException, IOException {
        requestAndTest(new URI("http://localhost:8080"), "Hello, world");

    }

    @Test
    public void shouldSayHammock() throws URISyntaxException, IOException {
        requestAndTest(new URIBuilder(new URI("http://localhost:8080"))
                        .setParameter("name", "Hammock")
                        .build(),
                "Hello, Hammock");
    }

    private void requestAndTest(URI uri, String s) throws IOException {
        try (CloseableHttpResponse response = HttpClients.createMinimal().execute(new HttpGet(uri))) {
            assert EntityUtils.toString(response.getEntity()).equals(s);
        }
    }
}
