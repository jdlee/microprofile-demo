package com.steeplesoft.microprofile.helidon;

import io.helidon.microprofile.server.Server;

import java.io.IOException;
import java.util.logging.LogManager;

public final class Main {
    private Main() { }

    public static void main(final String[] args) throws IOException {
        Server.create().start();
    }
}
