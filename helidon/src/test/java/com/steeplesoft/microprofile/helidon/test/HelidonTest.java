package com.steeplesoft.microprofile.helidon.test;

import io.helidon.microprofile.server.Server;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HelidonTest {
    private static Server server;

    @BeforeClass
    public static void setup() {
        server = Server.create();
        server.start();
    }

    @AfterClass
    public static void shutdown() {
        if (server != null) {
            server.stop();
        }
    }

    @Test
    public void shouldSayWorld() throws URISyntaxException, IOException {
        requestAndTest(new URI("http://localhost:8080"), "Hello, world");

    }

    @Test
    public void shouldSayHelidon() throws URISyntaxException, IOException {
        requestAndTest(new URIBuilder(new URI("http://localhost:8080"))
                        .setParameter("name", "Helidon")
                        .build(),
                "Hello, Helidon");
    }

    private void requestAndTest(URI uri, String s) throws IOException {
        try (CloseableHttpResponse response = HttpClients.createMinimal().execute(new HttpGet(uri))) {
            assert EntityUtils.toString(response.getEntity()).equals(s);
        }
    }
}
