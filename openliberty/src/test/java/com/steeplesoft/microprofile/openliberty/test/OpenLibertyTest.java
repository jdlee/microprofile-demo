package com.steeplesoft.microprofile.openliberty.test;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class OpenLibertyTest {
    public static final String URL = "http://localhost:8080/";

    @Test
    public void shouldSayWorld() throws URISyntaxException, IOException {
        requestAndTest(new URI(URL), "Hello, world");

    }

    @Test
    public void shouldSayOpenLiberty() throws URISyntaxException, IOException {
        requestAndTest(new URIBuilder(new URI(URL))
                        .setParameter("name", "OpenLiberty")
                        .build(),
                "Hello, OpenLiberty");
    }

    private void requestAndTest(URI uri, String s) throws IOException {
        System.out.println("Connecting to " + uri.toString());
        try (CloseableHttpResponse response = HttpClients.createMinimal().execute(new HttpGet(uri))) {
            Assertions.assertThat(EntityUtils.toString(response.getEntity()))
                    .isEqualTo(s);
        }
    }
}
