package com.steeplesoft.microprofile.payara.test;

import com.steeplesoft.microprofile.common.HelloWorldService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@RunWith(Arquillian.class)
@RunAsClient
public class HelloWorldResourceTest {
    @ArquillianResource
    private URL deploymentURL;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackage(HelloWorldService.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void shouldSayWorld() throws URISyntaxException, IOException {
        requestAndTest(deploymentURL.toURI(), "Hello, world");
    }

    @Test
    public void shouldSayPayara() throws URISyntaxException, IOException {
        requestAndTest(new URIBuilder(deploymentURL.toURI())
                .setParameter("name", "Payara").build(),
                "Hello, Payara");
    }

    private void requestAndTest(URI uri, String s) throws IOException {
        try (CloseableHttpResponse response = HttpClients.createMinimal().execute(new HttpGet(uri))) {
            assert EntityUtils.toString(response.getEntity()).equals(s);
        }
    }
}
