package com.steeplesoft.microprofile.thorntail;

import com.steeplesoft.microprofile.common.HelloWorldResource;
import com.steeplesoft.microprofile.common.HelloWorldService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class InjectionTest {
    @Inject
    private HelloWorldService service;
    @Inject
    private HelloWorldResource resource;

    @Deployment(name = "injectionstest")
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackage(HelloWorldService.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void verifyInjection() {
        assert service != null;
        assert resource != null;
    }

    @Test
    public void serviceSaysHelloCorrectly() {
        assert "Hello, Test".equals(service.sayHello("Test"));
    }

    @Test
    public void resourceSaysHelloCorrectly() {
        assert "Hello, Test".equals(resource.sayHello("Test"));
    }
}
